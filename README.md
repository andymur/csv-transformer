## How To Build And Run

Build is rather simple

> mvn clean install

After that please copy built transformer.jar from target directory to the project root directory.

You could run using only mandatory parameters (path to the all 4 files)

Example:

> java -jar -Dfile.data=inputs.csv -Dfile.output=output.csv -Dfile.filter=filter.csv -Dfile.columns=column-filter.csv transformer.jar

Or use configuration file (additional parameters could be added, see next sub section)

> java -jar -DconfigurationFile=config.properties transformer.jar

### Run Parameters

There are four mandatory parameters.

Other parameters are not mandatory and could be calculated based on your system (number or processors, available heap size).

- file.data is the path to the original data file
- file.ouput is the path to the final file
- file.filter is the path to control id filtering file
- file.columns is the path to the control column filtering file

There are additional parameters related to memory

- memory.available is value of memory transformer tries to use (XMX / 2 by default)
- memory.block is block of data read in one reading (default value is 10MB)

There are additional parameters related to threads

- threads.all number of threads will be used by transformer (by default is equal to number of cores)
- threads.read should be set only for multi-async reader. Number of threads for reading
- threads.filter number of threads used for processing the data.

And one additional parameter, you could choose reader implementation (Async is used by default), see section below.

- reader.implementation (should be async, multi-async or buffered, async is default)

## How To Generate Test Data

You are able to generate input data (data file and two control files).

Please run

> mvn clean -Dtest=InputGeneratorRunner#generate test

Now all file parameters are hardcoded:

- number of rows of data file (it is 10 millions)
- percent of columns to retain (20%)
- percent of ids to retain (10%)

Also file names:

- data file inputs.csv
- id filtration file filter.csv
- column filtration file column-filter.csv
- result file ouput.csv

All files will be generated in the project root directory.

## About Reader Implementations

We have three reader implementations

- Buffered. It is reader which reads blocks one by one in sync. way
- Multi Async. It is reader which reads blocks in parallel using async reading
- Async (Default). It is reader which reads data in single thread but use many bufferes and async. reading.


Proper implementation could be chosen at the start by given -Dimplementation propery or config file property.