package com.andymur.java.pg.writers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Final file writer.
 *
 * @author andymur
 *
 */
public class Writer implements Runnable {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private final BlockingQueue<String> buffer;
    private final Path outputFile;

    /**
     * Constructor.
     *
     * @param buffer to read result rows from.
     * @param outputFile result file.
     */
    public Writer(BlockingQueue<String> buffer, Path outputFile) {
        this.buffer = buffer;
        this.outputFile = outputFile;
    }

    @Override
    public void run() {
        try {

            while(true) {
                String row = buffer.poll(3, TimeUnit.SECONDS);

                Files.write(outputFile,
                        row.getBytes(), StandardOpenOption.WRITE,
                        StandardOpenOption.APPEND);
            }

        } catch (InterruptedException e) {
            logger.error("Error while taking or end of queue", e);
        } catch (IOException e) {
            logger.error("Error while writing", e);
        }
    }
}
