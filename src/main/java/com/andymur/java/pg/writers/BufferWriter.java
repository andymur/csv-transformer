package com.andymur.java.pg.writers;

import com.andymur.java.pg.transform.Converter;
import com.andymur.java.pg.transform.RowFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Operates with inner queue of original data file rows, filter them. <br/>
 *
 * Also counts number of read data and stops everything when data file totally read.
 *
 * @author andymur
 *
 */
public class BufferWriter {

    public Logger logger = LoggerFactory.getLogger(getClass());

    public final AtomicLong bytesRead = new AtomicLong(0L);
    public final AtomicInteger percentCounter = new AtomicInteger(0);

    public final AtomicBoolean canStop;
    private final long inputFileSize;
    private final BlockingQueue<String> buffer;

    private RowFilter rowFilter;
    private Converter converter;


    /**
     * Constructs writer.
     *
     * @param inputFileSize size of original data file.
     * @param canStop special marker for stop the process.
     * @param buffer queue for the final writer.
     */
    public BufferWriter(long inputFileSize, AtomicBoolean canStop,
                        BlockingQueue<String> buffer) {
        this.inputFileSize = inputFileSize;
        this.canStop = canStop;
        this.buffer = buffer;
    }

    /**
     * Transform the original row and send it to the final writer.
     *
     * @param row original row.
     * @throws InterruptedException
     */
    public void write(String row) throws InterruptedException {
        buffer.put(row.concat("\n"));
    }

    /**
     * Counts how much data is processed.
     *
     * @param readPortion data already processed.
     */
    public void countRead(int readPortion) {
        if (bytesRead.addAndGet(readPortion) >= inputFileSize) {
            canStop.set(true);
        }

        int percents = (int) ((bytesRead.get() * 100) / inputFileSize);
        if (percentCounter.get() < percents) {
            percentCounter.set(percents);
            logger.info("{}% transformed", percents);
        }
    }

}
