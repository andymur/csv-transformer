package com.andymur.java.pg.writers;

import com.andymur.java.pg.domain.Row;
import com.andymur.java.pg.readers.ControlMapReader;
import com.andymur.java.pg.transform.ColumnConverter;
import com.andymur.java.pg.transform.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Writer which aim is to convert and write header from original file to the result one.
 *
 * @author andymur
 *
 */
public class HeaderWriter {

    private static Logger logger = LoggerFactory.getLogger(HeaderWriter.class);

    private HeaderWriter() {
        throw new IllegalStateException("HeaderWriter cannot be instantiated");
    }

    /**
     * Converts and writes the header.
     *
     * @param columnFile column filter file.
     * @param inputFile original data file.
     * @param outputFile result file.
     * @return column converter (filter) instance.
     * @throws IOException
     */
    public static Converter writeHeader(Path columnFile,
                                   Path inputFile,
                                   Path outputFile) throws IOException {
        logger.debug("writeHeader.enter;");
        Files.write(outputFile, new byte[] {},
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

        String header;

        try (BufferedReader reader = Files.newBufferedReader(inputFile)) {
            header = reader.readLine();
        }

        List<String> headerColumns = Arrays.asList(header.split(Row.DELIMITER));

        Map<String, String> columnMapping = ControlMapReader.read(columnFile);

        List<Integer> remainedIndices = new ArrayList<>();

        for (int i = 0; i < headerColumns.size(); i++) {
            if (columnMapping.containsKey(headerColumns.get(i))) {
                remainedIndices.add(i);
            }
        }

        Converter columnConverter = new ColumnConverter();
        ((ColumnConverter) columnConverter).setRemainedColumnIndices(remainedIndices);

        Files.write(outputFile,
                headerColumns.stream()
                        .map(columnMapping::get)
                        .filter(Objects::nonNull)
                        .collect(Collectors.joining(Row.DELIMITER))
                        .concat("\n").getBytes()
        );

        logger.debug("writeHeader.exit;");
        return columnConverter;
    }
}
