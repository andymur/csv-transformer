package com.andymur.java.pg.tasks;

import com.andymur.java.pg.transform.Converter;
import com.andymur.java.pg.transform.RowFilter;
import com.andymur.java.pg.writers.BufferWriter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;

/**
 * Launcher for the QueueReader tasks
 *
 * @author andymur
 */
public class QueueReaderLauncher {
    private final BufferWriter bufferWriter;
    private final BlockingQueue<String> rowQueue;
    private RowFilter filter;
    private Converter converter;
    private ExecutorService executor;

    /**
     *
     * Constructor
     *
     * @param executor to work on.
     * @param filter id filter
     * @param converter column converter
     * @param bufferWriter buffer to write data file rows.
     * @param rowQueue to read data file rows from.
     */
    public QueueReaderLauncher(ExecutorService executor, RowFilter filter, Converter converter,
                               BufferWriter bufferWriter, BlockingQueue<String> rowQueue) {
        this.bufferWriter = bufferWriter;
        this.rowQueue = rowQueue;
        this.executor = executor;
        this.filter = filter;
        this.converter = converter;
    }

    public void launch() {
        executor.submit(new QueueReaderTask(filter, converter, bufferWriter, bufferWriter.canStop, rowQueue));
        executor.submit(new QueueReaderTask(filter, converter, bufferWriter, bufferWriter.canStop, rowQueue));
    }
}
