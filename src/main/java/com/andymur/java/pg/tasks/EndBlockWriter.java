package com.andymur.java.pg.tasks;

import com.andymur.java.pg.domain.Pair;
import com.andymur.java.pg.writers.BufferWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Writer for handling row endings.
 *
 * When we read the block of data it could contain a part of row in the end and in the beginning.<br/>
 *
 * We have to handle such row endings. For this purpose special map is used. <br/>
 *
 * This map contains block number and part of the row in the beginning and at the end of this block.<br/>
 *
 * By given block numbers we join adjacent blocks row beginning and ending into normal full row.
 *
 * @author andymur
 *
 */
public class EndBlockWriter {
    private Logger logger = LoggerFactory.getLogger(getClass());

    final ConcurrentHashMap<Integer, Pair<String, String>> endings;
    final int blockNumbers;
    final BufferWriter bufferWriter;

    /**
     *
     * Constructs it.
     *
     * @param blockNumbers number of blocks in the data file.
     * @param endings block number to partial rows pair.
     * @param bufferWriter writer to put normal rows.
     */
    public EndBlockWriter(int blockNumbers,
                          ConcurrentHashMap<Integer, Pair<String, String>> endings,
                          BufferWriter bufferWriter) {
        this.endings = endings;
        this.blockNumbers = blockNumbers;
        this.bufferWriter = bufferWriter;
    }

    public void launch() {
        List<Integer> blockHandled = IntStream.range(0, blockNumbers - 1).boxed().collect(Collectors.toList());
        try {
            while (!blockHandled.isEmpty()) {
                List<Integer> remainedBlocks = new ArrayList<>(blockHandled);

                for (int blockNumber : remainedBlocks) {

                    Pair<String, String> topPair = endings.get(blockNumber);
                    Pair<String, String> bottomPair = endings.get(blockNumber + 1);

                    if (topPair != null && bottomPair != null) {
                        String row = topPair.second.concat(bottomPair.first);
                        bufferWriter.countRead(bytes(row) + 1);
                        bufferWriter.write(row);

                        //We never write first from very first ending (cause it's a header) but we need to calculate it
                        if (blockNumber == 0) {
                            row = topPair.first;
                            bufferWriter.countRead(bytes(row) + 1);
                        }

                        blockHandled.remove((Object) blockNumber);
                    }

                }

            }
        } catch (InterruptedException e) {
            logger.error("interrupted while handling end of blocks", e);
        }
    }

    private int bytes(String s) {
        return s.getBytes().length;
    }
}
