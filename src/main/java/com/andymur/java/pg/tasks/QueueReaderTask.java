package com.andymur.java.pg.tasks;


import com.andymur.java.pg.transform.Converter;
import com.andymur.java.pg.transform.RowFilter;
import com.andymur.java.pg.writers.BufferWriter;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Task for reading data file row queue and put it to the appropriate writer.
 *
 * @author andymur
 */
public class QueueReaderTask implements Runnable {
    AtomicBoolean canStop;
    BlockingQueue<String> rowQueue;
    BufferWriter bufferWriter;
    RowFilter filter;
    Converter converter;

    /**
     * Constructor
     *
     * @param filter id filter
     * @param converter column filter
     * @param bufferWriter to put data file rows
     * @param canStop should we stop the tasks
     * @param rowQueue read data file rows from
     */
    public QueueReaderTask(RowFilter filter,
                           Converter converter,
                           BufferWriter bufferWriter,
                           AtomicBoolean canStop,
                           BlockingQueue<String> rowQueue) {
        this.filter = filter;
        this.converter = converter;
        this.rowQueue = rowQueue;
        this.canStop = canStop;
        this.bufferWriter = bufferWriter;
    }

    @Override
    public void run() {
        while (!canStop.get()) {
            try {

                String row = rowQueue.poll();
                if (row != null) {

                    // transform here
                    bufferWriter.countRead(row.getBytes().length + 1);

                    List<String> filtered = filter.filter(row);
                    Optional<String> convertedRow = converter.convert(filtered);

                    if (convertedRow.isPresent()) {
                        bufferWriter.write(convertedRow.get());
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
