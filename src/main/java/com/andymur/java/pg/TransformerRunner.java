package com.andymur.java.pg;

import com.andymur.java.pg.helpers.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 *
 * Transformation runner.
 *
 * @author andymur
 */
public class TransformerRunner {

    public static void main( String[] args ) throws IOException {
        PropertyReader propertyReader = new PropertyReader();
        new Transformer().transform(propertyReader);
    }

}
