package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Row;
import org.apache.commons.lang.RandomStringUtils;

/**
 * Used for test input generator
 *
 * @author andymur
 */
public class ColumnSubstitutor {

    private ColumnSubstitutor() {
        throw new IllegalStateException("ColumnSubstitutor cannot be instantiated");
    }

    /**
     * Generates column substitution string which contains original column plus its substitution.
     *
     * @param columnName original column
     * @return original column plus its substitution separated by the delimiter
     */
    public static final String generateColumnSubstitution(String columnName) {
        return columnName.concat(Row.DELIMITER)
                .concat(RandomStringUtils.randomAlphabetic(3).toUpperCase())
                .concat(RandomStringUtils.randomNumeric(1))
                .concat(Row.EOL);
    }
}
