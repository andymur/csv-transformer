package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Column;
import com.andymur.java.pg.domain.Row;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

/**
 * Generates test input data.
 *
 * @author andymur
 */
public class InputGenerator {

    /**
     * Generates data file, column filtering file and id filtering file by number of rows of data file and percents of filtration.
     *
     * @param dataFile main data file path.
     * @param columnsFile file path of the file with column filtering.
     * @param filtrationFile file path of the file with id filtering.
     * @param rowsNumber number of rows in data file to generate.
     * @param firstColumnIncluded whether header is included into rowsNumber.
     * @param columnsPercent percent of columns to generate in filtering file.
     * @param rowsPercent percent of id rows to generate in filtering file.
     */
    void generate(Path dataFile, Path columnsFile, Path filtrationFile,
                  List<Column> columns,
                  int rowsNumber,
                  boolean firstColumnIncluded,
                  int columnsPercent,
                  int rowsPercent) throws IOException {

        RowGenerator rowGenerator = new RowGenerator(columns);

        int rowNthIndex = nthIndex(rowsPercent);
        int colNthIndex = nthIndex(columnsPercent);

        try (
                BufferedWriter dataFileWriter = Files.newBufferedWriter(dataFile, StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING);
                BufferedWriter columnsFileWriter = Files.newBufferedWriter(columnsFile, StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING);
                BufferedWriter filtrationFileWriter = Files.newBufferedWriter(filtrationFile, StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING);
        ) {

            for (int i = firstColumnIncluded ? 0 : 1; i < columns.size(); i++) {
                if ((i % colNthIndex == 0)) {
                    columnsFileWriter.write(ColumnSubstitutor.generateColumnSubstitution(columns.get(i).getName()));
                }
            }

            Row rowForHeader = rowGenerator.generate();
            dataFileWriter.write(rowForHeader.columnsHeader());

            for (int i = 0; i < rowsNumber; i++) {
                Row row = rowGenerator.generate();
                dataFileWriter.write(stringify(row));

                if (i % rowNthIndex == 0) {
                    filtrationFileWriter.write(IdSubstitutor.generateIdSubstitution(id(row)));
                }
            }
        }
    }

    private int nthIndex(int percent) {
        return (int) (1 / (percent / 100.));
    }

    private String stringify(Row row) {
        return row.toString();
    }

    private String id(Row row) {
        return Arrays.stream(row.values()).findFirst().get().toString();
    }


}
