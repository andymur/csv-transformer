package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Column;
import com.andymur.java.pg.domain.Row;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Row generator, used for test input file generation only.
 *
 * @author andymur
 */
public class RowGenerator {
    List<Column> columns;

    /**
     * Generator constructor.
     *
     * @param columns generated file header columns.
     */
    public RowGenerator(
            List<Column> columns) {
        this.columns = new ArrayList<>(columns);
    }

    /**
     * Generates random row. Row contains columns according to generator header and random values inside.
     * @return random row
     */
    public Row generate() {
        List<Object> values = new ArrayList<>(columns.size());

        for (Column column: columns) {
            int randomInt = RandomUtils.nextInt(5) + 5;
            switch (column.getType()) {
                case STRING:
                    values.add(RandomStringUtils.randomAlphabetic(randomInt));
                    break;
                case INTEGER:
                    values.add(randomInt * RandomUtils.nextInt(10) + 1);
                    break;
                case DOUBLE:
                    values.add(RandomUtils.nextDouble());
                    break;
                case LONG:
                    values.add(RandomUtils.nextLong());
                    break;
                case BOOLEAN:
                    values.add(RandomUtils.nextBoolean());
            }
        }

        return new Row(columns, values.toArray());
    }
}
