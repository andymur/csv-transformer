package com.andymur.java.pg.helpers;

import com.andymur.java.pg.readers.AbstractDataReader.DataReaderImplementation;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Helps work with properties from VM or configuration file.
 *
 * @author andymur
 */
public class PropertyReader {

    private static final String DATA_FILE = "file.data";
    private static final String FILTER_FILE = "file.filter";
    private static final String COLUMN_FILTER_FILE = "file.columns";
    private static final String OUTPUT_FILE = "file.output";

    private static final String BLOCK_SIZE = "memory.block";
    private static final String MEMORY_AVAILABLE = "memory.available";

    private static final String READER_IMPLEMENTATION = "reader.implementation";

    private static final String ALL_THREADS = "threads.all";
    private static final String READ_THREADS = "threads.read";
    private static final String FILTER_THREADS = "threads.filter";
    public static final String DEFAULT_BLOCK_SIZE = "10M";

    Properties properties;

    /**
     * Default constructor. Tries to read properties from the default destination.
     *
     * @throws IOException
     */
    public PropertyReader() throws IOException {
        this.properties = readProperties();
    }

    /**
     * Construct reader with given default properties.
     *
     * @param properties default properties
     */
    public PropertyReader(Properties properties) {
        this.properties = properties;
    }

    /**
     * Reads and returns data file name property.
     *
     * @return data file name.
     */
    public String getDataFileName() {
        return (String) propertyValue(properties, DATA_FILE);
    }

    /**
     * Reads and returns output file name property.
     *
     * @return output file name.
     */
    public String getResultFileName() {
        return (String) propertyValue(properties, OUTPUT_FILE);
    }

    /**
     * Reads and returns filter file name property.
     *
     * @return filter file name.
     */
    public String getFilterFileName() {
        return (String) propertyValue(properties, FILTER_FILE);
    }

    /**
     * Reads and returns column filter file name property.
     *
     * @return column filter file name.
     */
    public String getColumnFileName() {
        return (String) propertyValue(properties, COLUMN_FILTER_FILE);
    }

    /**
     * Reads and returns available memory value. Default value is XMX / 2.
     *
     * @return available memory value.
     */
    public long getAvailableMemory() {
        Long available = toBytesFromString((String) propertyValue(properties, MEMORY_AVAILABLE));

        if (available == null) {
            return Runtime.getRuntime().maxMemory() / 2;
        }

        return available;
    }

    /**
     * Reads and returns block size property. Default value is 10MB.
     *
     * @return block size value.
     */
    public int getBlockSize() {
        Long size = toBytesFromString((String) propertyValue(properties, BLOCK_SIZE));

        if (size == null || size <= 0) {
            size = toBytesFromString(DEFAULT_BLOCK_SIZE);
        }

        return size.intValue();
    }

    /**
     * Reads and returns data reader implementation property. Default value is ASYNC.
     *
     * @return data reader implementation type.
     */
    public DataReaderImplementation getReaderImplementation() {

        if (propertyValue(properties, READER_IMPLEMENTATION) == null) {
            return DataReaderImplementation.ASYNC;
        }

        switch ((String) propertyValue(properties, READER_IMPLEMENTATION)) {
            case "multi-async":
                return DataReaderImplementation.MULTI_ASYNC;
            case "async":
                return DataReaderImplementation.ASYNC;
            default:
                return DataReaderImplementation.BUFFERED;
        }
    }

    /**
     * Reader and returns all threads property. Default value is core number.
     *
     * @return all available threads value.
     */
    public int getAllThreadsNumber() {
        Integer allThreads = propertyValue(properties, ALL_THREADS) != null
                ? Integer.valueOf((String) propertyValue(properties, ALL_THREADS))
                : null;

        return allThreads != null
                ? allThreads :
                Runtime.getRuntime().availableProcessors();
    }

    /**
     * Reads and returns number of threads for reading property.
     *
     * @return number of reading threads. (Make sense of Multi Async only)
     */
    public Integer getReadThreadsNumber() {
        return propertyValue(properties, READ_THREADS) != null
                ? Integer.valueOf((String) propertyValue(properties, READ_THREADS))
                : null;
    }

    /**
     * Reads and returns number of threads for filtration queue.
     *
     * @return number of threads for filtration.
     */
    public Integer getFilterThreadsNumber() {
        return propertyValue(properties, FILTER_THREADS) != null
                ? Integer.valueOf((String) propertyValue(properties, FILTER_THREADS))
                : null;
    }


    static Object propertyValue(Properties properties,
                                String propertyName) {
        return System.getProperty(propertyName) != null
                ? System.getProperty(propertyName)
                : properties.getProperty(propertyName);
    }

    static Properties readProperties() throws IOException {
        String propertyFile = (String) propertyValue(new Properties(), "configurationFile");
        Properties properties = new Properties();


        if (propertyFile != null) {
            try (InputStream inputStream = Files.newInputStream(Paths.get(propertyFile))) {
                properties.load(inputStream);
            }
        }

        return properties;
    }

    static Long toBytesFromString(String memory) {

        if (memory == null) {
            return null;
        }

        String mbOrGb = memory.substring(memory.length() - 1, memory.length()).toUpperCase();

        int value = Integer.valueOf(memory.substring(0, memory.length() - 1));

        switch (mbOrGb) {
            case "M":
                return value * 1024l * 1024l;
            case "G":
                return value * 1024l * 1024l * 1024l;
            default:
                throw new IllegalArgumentException("Should be megabytes or gigabytes");
        }
    }
}
