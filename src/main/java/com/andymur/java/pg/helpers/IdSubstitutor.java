package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Row;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

/**
 * Used for test input generator
 *
 * @author andymur
 */
public class IdSubstitutor {

    private IdSubstitutor() {
        throw new IllegalStateException("IdSubstitutor cannot be instantiated");
    }

    public static final String generateIdSubstitution(String id) {
        return id.concat(Row.DELIMITER)
                .concat(RandomStringUtils.randomAlphabetic(5 + RandomUtils.nextInt(5)))
                .concat(Row.EOL);
    }
}
