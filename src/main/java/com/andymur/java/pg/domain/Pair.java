package com.andymur.java.pg.domain;

/**
 * Simple pair representation
 * @param <T> first element of the Pair
 * @param <U> second element of the Pair
 *
 * @author andymur
 */
public class Pair<T, U> {
    public final T first;
    public final U second;

    private Pair(T first, U second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Constructs pair of two elements
     *
     * @param first element of the pair
     * @param second element of the pair
     * @param <T> first element type
     * @param <U> second element type
     * @return Pair
     */
    public static <T, U> Pair<T, U> of(T first, U second) {
        return new Pair<>(first, second);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Pair{");
        sb.append("first=").append(first);
        sb.append(", second=").append(second);
        sb.append('}');
        return sb.toString();
    }
}
