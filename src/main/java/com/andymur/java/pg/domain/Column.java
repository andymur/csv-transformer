package com.andymur.java.pg.domain;

/**
 * Represents column of the csv file. <br/>
 * Column has name and appropriate type (String, int, long, double, boolean).
 *
 * @author andymur
 */
public class Column {
    private final String name;
    private final DataType type;

    /**
     * Column constructor
     *
     * @param name column name
     * @param type column type
     */
    public Column(String name, DataType type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Data cell type
     */
    public static enum DataType {
        STRING, INTEGER, LONG, DOUBLE, BOOLEAN
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public DataType getType() {
        return type;
    }
}
