package com.andymur.java.pg.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents row of the csv file (data file). <br/>
 *
 * Row has several values. Also contains header of the its file.
 *
 * @author andymur
 */
public class Row {
    public static final String DELIMITER = "\t";
    public static final String EOL = "\n";

    List<Column> columns;
    Object[] values;

    /**
     * Row constructor
     *
     * @param columns row's files header columns
     * @param values row's values
     */
    public Row(List<Column> columns, Object[] values) {
        this.columns = columns;
        this.values = values;
    }

    @Override
    public String toString() {
        return Arrays.stream(values)
                .map(Object::toString)
                .collect(Collectors.joining(DELIMITER)).concat(EOL);
    }

    /**
     * Creates string which contains row's file header separated by delimiter
     * @return row's file header
     */
    public String columnsHeader() {
        return columns.stream()
                .map(Object::toString)
                .collect(Collectors.joining(DELIMITER)).concat(EOL);
    }

    public List<Column> columns() {
        return new ArrayList<>(columns);
    }

    public Object[] values() {
        return values;
    }
}
