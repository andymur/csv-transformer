package com.andymur.java.pg.transform;

import java.util.List;
import java.util.Optional;

/**
 * Converter which filters unwanted columns from the row.
 *
 * @author andymur
 *
 */
public interface Converter {

    /**
     * Converts filtered row, removes cells from unwanted columns.
     *
     * @param values filtered row cell values. Could be empty if all filtered.
     * @return row of the output file or empty if original row should be filtered.
     */
    Optional<String> convert(List<String> values);
}
