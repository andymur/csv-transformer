package com.andymur.java.pg.transform;

import com.andymur.java.pg.domain.Row;

import java.util.*;

/**
 * Default implementation.
 *
 * @author andymur
 */
public class RowIdFilter implements RowFilter {

    private Map<String, String> filterMap;

    /**
     * Set control file data.
     * @param filterMap control file data.
     */
    public void setFilterMap(Map<String, String> filterMap) {
        this.filterMap = filterMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> filter(String row) {
        List<String> valuesWithId = Arrays.asList(row.split(Row.DELIMITER));
        String id = id(valuesWithId);

        String newId = filterMap.get(id);

        if (newId != null) {
            valuesWithId.set(0, newId);
            return new ArrayList<>(valuesWithId);
        }

        return Collections.emptyList();
    }

    private String id(List<String> valuesWithId) {

        if (valuesWithId == null || valuesWithId.isEmpty()) {
            throw new IllegalArgumentException("Row cannot be empty or null");
        }

        return valuesWithId.get(0);
    }
}
