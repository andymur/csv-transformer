package com.andymur.java.pg.transform;

import java.nio.ByteBuffer;

/**
 * Processes raw data from byte buffers into rows (strings).
 *
 * Also handles line endings.
 *
 * @author andymur
 *
 */
public interface DataGrinder {
    /**
     * Processes raw data.
     *
     * @param blockNumber number of block in file. We need to know for handling line endings properly.
     * @param blockData block of raw data
     * @throws InterruptedException
     */
    void grind(int blockNumber, ByteBuffer blockData) throws InterruptedException;
}
