package com.andymur.java.pg.transform;

import com.andymur.java.pg.domain.Row;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Default converter implementation.
 *
 * @author andymur
 *
 */
public class ColumnConverter implements Converter {

    List<Integer> remainedColumnIndices;

    /**
     * Put column indices which should not be filtered.
     *
     * @param remainedColumnIndices column indices list.
     */
    public void setRemainedColumnIndices(List<Integer> remainedColumnIndices) {
        this.remainedColumnIndices = remainedColumnIndices;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> convert(List<String> values) {
        return values.isEmpty() ? Optional.empty()
                : Optional.of(IntStream.range(0, values.size())
                .filter(remainedColumnIndices::contains)
                .mapToObj(values::get)
                .collect(Collectors.joining(Row.DELIMITER)));
    }
}
