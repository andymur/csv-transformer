package com.andymur.java.pg.transform;

import com.andymur.java.pg.domain.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * {@inheritDoc}
 *
 * @author andymur
 *
 */
public class DataGrinderImpl implements DataGrinder {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private ConcurrentHashMap<Integer, Pair<String, String>> endings;
    private BlockingQueue<String> rowQueue;
    private final int blockNumbers;

    /**
     * Creates default data grinder implementation.
     *
     * @param endings data structure for line endings. (number of block -> (piece of the row from the start of the block, piece of the row from the end of the block))
     * @param rowQueue queue for full data rows
     * @param blockNumbers number of blocks of the data file
     */
    public DataGrinderImpl(ConcurrentHashMap<Integer, Pair<String, String>> endings,
                           BlockingQueue<String> rowQueue,
                           int blockNumbers) {
        this.endings = endings;
        this.rowQueue = rowQueue;
        this.blockNumbers = blockNumbers;
    }

    /**
     * Set special data structure for row endings.
     *
     * @param endings data structure for line endings. (number of block -> (piece of the row from the start of the block, piece of the row from the end of the block))
     */
    public void setEndings(ConcurrentHashMap<Integer, Pair<String, String>> endings) {
        this.endings = endings;
    }

    /**
     * Set queue to transport full normal rows.
     *
     * @param rowQueue queue for full data rows.
     */
    public void setRowQueue(BlockingQueue<String> rowQueue) {
        this.rowQueue = rowQueue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void grind(int blockNumber, ByteBuffer blockData) throws InterruptedException {
        logger.trace("grid.enter");
        byte[] content;

        if (blockData.hasArray()) {
            content = blockData.array();
        } else {
            content = new byte[blockData.remaining()];
            blockData.get(content);
        }

        String allInOne = new String(content, Charset.defaultCharset());
        boolean endsOnNL = allInOne.endsWith("\n");
        String[] allStrings = allInOne.split("\n");

        String prefix = allStrings[0];

        if (blockNumber != blockNumbers - 1) {
            String postfix = allStrings.length > 1 ? allStrings[allStrings.length - 1] : "";

            if (endsOnNL) {
                postfix = postfix.concat("\n");
            }

            endings.put(blockNumber, Pair.of(prefix, postfix));

            for (int i = 1; i < allStrings.length - 1; i++) {
                rowQueue.put(allStrings[i]);
            }
        } else {
            endings.put(blockNumber, Pair.of(prefix, ""));

            for (int i = 1; i < allStrings.length; i++) {
                rowQueue.put(allStrings[i]);
            }
        }


        logger.trace("grid.exit");
    }
}
