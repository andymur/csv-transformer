package com.andymur.java.pg.transform;

import java.util.List;

/**
 * Do the row filtration. <br/>
 *
 * Takes original row and returns list of cells with replaced first cell (it is replaced by new id) <br/>
 *
 * Returns empty list if row should be filtered.
 *
 * @author andymur
 *
 */
public interface RowFilter {
    /**
     * Filter original row and replace first cell with the new identifier.
     *
     * @param row original row.
     * @return list of cells with replaced first cell (it is replaced by new id) or empty if should be filtered.
     */
    List<String> filter(String row);
}
