package com.andymur.java.pg.readers;

import com.andymur.java.pg.transform.DataGrinder;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.*;

/**
 * Reader implementation which reads data asynchronously and in multiple threads.
 *
 * @author andymur
 */
public class MultiAsyncDataReader extends AbstractDataReader {

    private int threadNumber;

    /**
     * Set number of threads for reading the data.
     *
     * @param threadNumber number of threads to read data in.
     */
    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ReaderConfigurator createConfigurator() {
        return new MultiAsyncDataReaderConfigurator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void read(DataGrinder grinder, Path dataFile) throws IOException {

        ExecutorService service = Executors.newFixedThreadPool(threadNumber,
                new BasicThreadFactory.Builder()
                        .namingPattern("async-reader-thread-%d")
                        .build());

        Semaphore semaphore = new Semaphore(((MultiAsyncDataReaderConfigurator) configurator).getPermitsNumber());
        CountDownLatch latch = new CountDownLatch(configurator.getBlocksNumber());

        long fileSize = configurator.getFileSize();
        int blockNumbers = configurator.getBlocksNumber();
        int blockSize = configurator.getBlockSize();

        try (AsynchronousFileChannel channel = AsynchronousFileChannel.open(
                dataFile, StandardOpenOption.READ
        )) {

            for (int i = 0; i < blockNumbers; i++) {

                if (i < blockNumbers - 1) {
                    service.submit(new AsyncReaderTask(latch,
                            semaphore,
                            i,
                            blockSize,
                            (long) i * blockSize,
                            channel, grinder));
                } else {
                    service.submit(new AsyncReaderTask(latch, semaphore,
                            i,
                            (int) fileSize - i * blockSize,
                            (long) i * blockSize,
                            channel, grinder));
                }
            }

            latch.await();

        } catch (InterruptedException e) {
            logger.error("Interrupted while reading data", e);
        }

        service.shutdown();
    }

    static class AsyncReaderTask implements Runnable {

        private Logger logger = LoggerFactory.getLogger(getClass());

        private long position;
        private AsynchronousFileChannel channel;
        private int blockSize;
        private int blockNumber;
        private Semaphore semaphore;
        private CountDownLatch latch;
        private DataGrinder grinder;

        public AsyncReaderTask(CountDownLatch latch,
                               Semaphore semaphore,
                               int blockNumber,
                               int blockSize,
                               long position,
                               AsynchronousFileChannel channel,
                               DataGrinder grinder) {
            this.position = position;
            this.channel = channel;
            this.blockNumber = blockNumber;
            this.blockSize = blockSize;
            this.semaphore = semaphore;
            this.latch = latch;
            this.grinder = grinder;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();

                ByteBuffer buffer = ByteBuffer.allocate(blockSize);

                Future<Integer> integerFuture = channel.read(buffer, position);

                integerFuture.get();
                grinder.grind(blockNumber, buffer);
            } catch (InterruptedException | ExecutionException e) {
                logger.error("Error while reading piece of data", e);
            } finally {
                semaphore.release();
                latch.countDown();
            }
        }
    }
}
