package com.andymur.java.pg.readers;


import java.io.IOException;
import java.nio.file.Path;

/**
 * Reads data file, also contains configurator for related parameters.<br/>
 *
 * Main purpose is to read data source file.
 *
 * @author andymur
 */
public interface DataReader {
    /**
     * Reads source data file.
     *
     * @throws IOException
     */
    void read() throws IOException;

    /**
     * Returns reader configurator.
     *
     * @return configurator for reading.
     */
    ReaderConfigurator configurator();
}
