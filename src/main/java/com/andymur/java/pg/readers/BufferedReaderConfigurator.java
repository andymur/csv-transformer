package com.andymur.java.pg.readers;

/**
 * Configurator for buffered reader implementation. <br/>
 *
 * All parameters taken from based configurator.
 *
 * @author andymur
 */
public class BufferedReaderConfigurator extends BaseReaderConfigurator {
}