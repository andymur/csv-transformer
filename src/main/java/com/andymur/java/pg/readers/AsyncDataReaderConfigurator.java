package com.andymur.java.pg.readers;

/**
 * Configurator for async data reader implementation <br/>
 *
 * Besides common parameters from BaseReaderConfigurator it returns buffer number.<br/>
 *
 * Buffer number is the number of buffers used for reading data file.
 *
 * @author andymur
 */
public class AsyncDataReaderConfigurator extends BaseReaderConfigurator {

    public static final int QUOTIENT = 3;
    private int bufferNumber;

    public int getBufferNumber() {
        if (bufferNumber == 0) {
            bufferNumber = (int) (getAvailableMemory() / getBlockSize()) / QUOTIENT;
        }

        return bufferNumber > 1 ? bufferNumber : 2;
    }
}
