package com.andymur.java.pg.readers;

/**
 * Configurator for multi async data reader implementation. <br/>
 *
 * Adds permits number to based configurator parameters. <br/>
 *
 * Since multi async data reader reads data in parallel, it is constrained by number of permits. <br/>
 *
 * Reader is not permitted to read more than number of permits in parallel.
 *
 * @author andymur
 *
 */
public class MultiAsyncDataReaderConfigurator extends BaseReaderConfigurator {

    public static final int MULTI_ASYNC_PERMIT_QUOTIENT = 2;
    private int permitsNumber;

    public int getPermitsNumber() {

        if (permitsNumber == 0) {
            permitsNumber = (int) (getAvailableMemory() / (getBlockSize() / MULTI_ASYNC_PERMIT_QUOTIENT));
        }

        return permitsNumber > 1 ? permitsNumber : 2;
    }
}
