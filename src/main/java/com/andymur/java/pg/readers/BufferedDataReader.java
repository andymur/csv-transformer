package com.andymur.java.pg.readers;

import com.andymur.java.pg.transform.DataGrinder;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * Buffered reader implementation. <br/>
 *
 * It reads all the data consequently and synchronously using byte buffer. <br/>
 *
 * Buffer size set to block size, which is taken from configurator.
 *
 * @author andymur
 */
public class BufferedDataReader extends AbstractDataReader {

    /**
     * {@inheritDoc}
     */
    @Override
    protected BaseReaderConfigurator createConfigurator() {
        return new BaseReaderConfigurator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void read(DataGrinder grinder, Path dataFile) throws IOException {
        try (FileChannel channel = FileChannel.open(dataFile, StandardOpenOption.READ)) {

            for (int i = 0; i < configurator.getBlocksNumber(); i++) {

                ByteBuffer buffer;
                int bufferSize = bufferSize(i);
                buffer = ByteBuffer.allocate(bufferSize);
                channel.read(buffer);

                try {
                    grinder.grind(i, buffer);
                } catch (InterruptedException e) {
                    logger.error("Error while grinding", e);
                }
            }

        }
    }
}
