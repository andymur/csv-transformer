package com.andymur.java.pg.readers;

import com.andymur.java.pg.domain.Row;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * Reader for control files (filtering file with columns and id filtering file).
 *
 * @author andymur
 */
public class ControlMapReader {

    private ControlMapReader() {
        throw new IllegalStateException("ControlMapReader cannot be instantiated");
    }

    /**
     * Reads and returns control information
     *
     * @param inputFile control file
     * @return control information for filtering.
     * @throws IOException
     */
    public static Map<String, String> read(Path inputFile) throws IOException {
        Map<String, String> result = new HashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(inputFile)) {
            String line;

            while ((line = reader.readLine()) != null) {
                String[] row = line.split(Row.DELIMITER);
                result.put(row[0], row[1]);
            }
        }

        return result;
    }
}
