package com.andymur.java.pg.readers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Base implementation
 *
 * @author andymur
 */
public class BaseReaderConfigurator implements ReaderConfigurator {
    Logger logger = LoggerFactory.getLogger(getClass());

    private long fileSize;
    private long availableMemory;
    private int blockSize;
    private int blockNumbers;
    private Path dataFile;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAvailableMemory(long availMemory) {
        this.availableMemory = availMemory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Path getDataFile() {
        return dataFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure(Path inputFile) throws IOException {
        fileSize = Files.size(inputFile);
        this.dataFile = inputFile;

        if (availableMemory > fileSize) {
            availableMemory = fileSize;
        }

        if (blockSize > availableMemory) {
            blockSize = (int) availableMemory / 2;
        }

        logger.info("configure; available memory = {}, file size = {}, block size = {}, block numbers = {}",
                availableMemory, fileSize, blockSize, getBlocksNumber());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getBlocksNumber() {

        if (fileSize == 0) {
            throw new IllegalStateException("Configurator has not been initialized. Please configure it first.");
        }

        if (blockNumbers == 0) {
            blockNumbers = fileSize % blockSize == 0
                    ? (int) (fileSize / blockSize)
                    : (int) (fileSize / blockSize) + 1;
        }

        return blockNumbers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getFileSize() {
        return fileSize;
    }

    /**
     *
     * Reads and returns memory should be used in bytes
     *
     *@return used memory in bytes
     */
    public long getAvailableMemory() {
        return availableMemory;
    }

    @Override
    public int getBlockSize() {
        return blockSize;
    }
}
