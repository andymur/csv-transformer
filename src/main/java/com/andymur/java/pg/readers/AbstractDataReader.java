package com.andymur.java.pg.readers;

import com.andymur.java.pg.transform.DataGrinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.annotation.Inherited;
import java.nio.file.Path;

/**
 * Provides common scenario for readers for transformation task.
 *
 * All transformation readers use DataGrinder for further processing of read data.
 *
 */
public abstract class AbstractDataReader implements DataReader {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected DataGrinder grinder;

    protected ReaderConfigurator configurator = createConfigurator();

    /**
     * Set grinder
     * @param grinder grinder for processing raw data
     */
    public void setGrinder(DataGrinder grinder) {
        this.grinder = grinder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void read() throws IOException {
        if (configurator == null || configurator.getBlocksNumber() == 0) {
            throw new IllegalStateException("Reader is not initialized, please init first");
        }
        read(grinder, configurator.getDataFile());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReaderConfigurator configurator() {
        return configurator;
    }

    /**
     * Creates configurator related to reader implementation
     * @return reader configurator implementation
     */
    abstract protected ReaderConfigurator createConfigurator();

    /**
     * Reads data from data file and process it with the grinder.
     *
     * @param grinder grinder to process raw data
     * @param dataFile input data file
     * @throws IOException
     */
    abstract protected void read(DataGrinder grinder, Path dataFile) throws IOException;


    public enum DataReaderImplementation {
        MULTI_ASYNC, BUFFERED, ASYNC
    }

    protected int bufferSize(int blockNumber) {
        int blockNumbers = configurator.getBlocksNumber();
        int blockSize = configurator.getBlockSize();
        long fileSize = configurator.getFileSize();
        if (blockNumber < blockNumbers - 1) {
            return blockSize;
        } else {
            return (int) (fileSize - blockNumber * blockSize);
        }
    }
}
