package com.andymur.java.pg.readers;

import com.andymur.java.pg.domain.Pair;
import com.andymur.java.pg.transform.DataGrinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Reader implementation which uses async io for reading file in single thread. <br/>
 *
 * It has number of buffers to which data being read from file.<br/>
 *
 * All read operations perform in asynchronous way.
 *
 * @author andymur
 */
public class AsyncDataReader extends AbstractDataReader {

    /**
     * {@inheritDoc}
     */
    @Override
    protected ReaderConfigurator createConfigurator() {
        return new AsyncDataReaderConfigurator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void read(DataGrinder grinder, Path dataFile) throws IOException {

        int counter = 0;
        int bufferNumber = ((AsyncDataReaderConfigurator) configurator).getBufferNumber();

        Map<Integer, Pair<ByteBuffer, Future<Integer>>> futureBuffers = IntStream.range(0, bufferNumber)
                .boxed()
                .collect(Collectors.toMap(i -> i, i -> Pair.of(null, null)));

        try (AsynchronousFileChannel channel = AsynchronousFileChannel.open(dataFile, StandardOpenOption.READ)) {
            while (counter < configurator.getBlocksNumber()) {

                Set<Integer> indices = new HashSet<>(futureBuffers.keySet());
                for (int index: indices) {

                    if (counter >= configurator.getBlocksNumber()) {
                        break;
                    }

                    int bufferSize = bufferSize(counter);

                    Pair<ByteBuffer, Future<Integer>> bufferFuture = futureBuffers.get(index);

                    ByteBuffer buffer = bufferFuture.first;
                    Future<Integer> future = bufferFuture.second;

                    if (future == null || future.isDone()) {

                        if (future != null) {
                            try {
                                grinder.grind(index, buffer);
                            } catch (InterruptedException e) {
                                logger.error("Error while grinding", e);
                            }
                        }

                        futureBuffers.remove(index);

                        buffer = ByteBuffer.allocate(bufferSize);
                        future = channel.read(buffer, Long.valueOf(counter) * configurator.getBlockSize());
                        futureBuffers.put(counter, Pair.of(buffer, future));
                        counter += 1;
                    }

                }
            }

            while (!futureBuffers.isEmpty()) {
                Set<Integer> indices = new HashSet<>(futureBuffers.keySet());
                for (int index : indices) {
                    Pair<ByteBuffer, Future<Integer>> bufferFuture = futureBuffers.get(index);

                    ByteBuffer buffer = bufferFuture.first;
                    Future<Integer> future = bufferFuture.second;

                    if (future.isDone()) {
                        try {
                            grinder.grind(index, buffer);
                        } catch (InterruptedException e) {
                            logger.error("Error while grinding", e);
                        }
                        futureBuffers.remove(index);
                    }
                }
            }
        }
    }
}
