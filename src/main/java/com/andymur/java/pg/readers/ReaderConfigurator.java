package com.andymur.java.pg.readers;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Configurator which calculates inside of it and returns all needed parameters for data file reading.
 *
 * @author andymur
 */
public interface ReaderConfigurator {

    /**
     * Calculates all configuration parameters.
     *
     * @param inputFile data file.
     * @throws IOException
     */
    void configure(Path inputFile) throws IOException;

    /**
     * Returns number of blocks.
     *
     * @return number of blocks.
     */
    int getBlocksNumber();

    /**
     *
     * Set available memory.
     *
     * @param availableMemory memory to use.
     */
    void setAvailableMemory(long availableMemory);

    /**
     *
     * Set block size.
     *
     * @param blockSize for reading
     */
    void setBlockSize(int blockSize);

    /**
     * Returns path of data file.
     *
     * @return path of data file.
     */
    Path getDataFile();

    /**
     *
     * Returns block size.
     *
     * @return block size.
     */
    int getBlockSize();

    /**
     *
     * Returns data file size.
     *
     * @return data file size.
     */
    long getFileSize();
}
