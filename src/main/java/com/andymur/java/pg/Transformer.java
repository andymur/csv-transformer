package com.andymur.java.pg;

import com.andymur.java.pg.domain.Pair;
import com.andymur.java.pg.helpers.PropertyReader;
import com.andymur.java.pg.readers.*;
import com.andymur.java.pg.tasks.EndBlockWriter;
import com.andymur.java.pg.tasks.QueueReaderLauncher;
import com.andymur.java.pg.transform.*;
import com.andymur.java.pg.writers.BufferWriter;
import com.andymur.java.pg.writers.HeaderWriter;
import com.andymur.java.pg.writers.Writer;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.andymur.java.pg.readers.AbstractDataReader.DataReaderImplementation.ASYNC;
import static com.andymur.java.pg.readers.AbstractDataReader.DataReaderImplementation.MULTI_ASYNC;

/**
 * Main class of the transformation.
 *
 * @author andymur
 *
 */
public class Transformer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final int QUEUE_DEFAULT_SIZE = 10000;

    /**
     * Makes the transformation.
     *
     * @param propertyReader reader for getting all the properties
     * @throws IOException
     */
    public void transform(PropertyReader propertyReader) throws IOException {
        Path outputFile = Paths.get(propertyReader.getResultFileName());

        Converter columnConverter = writeHeader(propertyReader);
        Map<String, String> filterData = readFilterData(propertyReader);

        //set up reader
        DataReader reader = getDataReader(propertyReader);

        long fileSize = reader.configurator().getFileSize();
        int blockNumbers = reader.configurator().getBlocksNumber();

        //set up data channels
        BlockingQueue<String> rowQueue = new ArrayBlockingQueue<>(QUEUE_DEFAULT_SIZE);
        ConcurrentHashMap<Integer, Pair<String, String>> endings = new ConcurrentHashMap<>();
        BlockingQueue<String> buffer = new ArrayBlockingQueue<>(QUEUE_DEFAULT_SIZE);

        //set up grinder
        DataGrinder grinder = new DataGrinderImpl(endings, rowQueue, blockNumbers);
        ((AbstractDataReader) reader).setGrinder(grinder);

        //set up filter
        RowFilter filter = getRowFilter(filterData);

        BufferWriter bufferWriter = new BufferWriter(fileSize, new AtomicBoolean(false), buffer);

        // start all the work
        runTransformation(propertyReader, outputFile, reader, bufferWriter, blockNumbers,
                rowQueue, buffer, endings, filter, columnConverter);
    }

    private void runTransformation(PropertyReader propertyReader, Path outputFile, DataReader reader,
                                   BufferWriter bufferWriter,
                                   int blockNumbers,
                                   BlockingQueue<String> rowQueue,
                                   BlockingQueue<String> buffer,
                                   ConcurrentHashMap<Integer,
                                   Pair<String, String>> endings,
                                   RowFilter filter,
                                   Converter converter) throws IOException {
        long startedAt = System.currentTimeMillis();
        logger.info("Transformation started at: {}", LocalDateTime.now());

        ExecutorService executor = Executors.newFixedThreadPool(getFilterThreadsNumber(propertyReader),
                new BasicThreadFactory.Builder()
                        .namingPattern("row-queue-reader-thread-%d")
                        .build());

        new QueueReaderLauncher(executor, filter, converter, bufferWriter, rowQueue).launch();
        executor.submit(new Writer(buffer, outputFile));

        reader.read();

        new EndBlockWriter(blockNumbers, endings, bufferWriter).launch();

        executor.shutdown();

        while (!executor.isTerminated());
        logger.info("Transformation finished at: {}, spent {}s", LocalDateTime.now(),
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startedAt));
    }

    private Map<String, String> readFilterData(PropertyReader propertyReader) throws IOException {
        return ControlMapReader.read(Paths.get(propertyReader.getFilterFileName()));
    }

    private DataReader getDataReader(PropertyReader propertyReader) throws IOException {

        AbstractDataReader.DataReaderImplementation implementation = propertyReader.getReaderImplementation();
        DataReader reader;

        if (MULTI_ASYNC == implementation) {
            reader = new MultiAsyncDataReader();
            ((MultiAsyncDataReader) reader).setThreadNumber(getReadThreadsNumber(propertyReader));
        } else if (ASYNC == implementation) {
            reader = new AsyncDataReader();
        } else {
            reader = new BufferedDataReader();
        }

        ReaderConfigurator configurator = reader.configurator();

        configurator.setAvailableMemory(propertyReader.getAvailableMemory());
        configurator.setBlockSize(propertyReader.getBlockSize());

        configurator.configure(Paths.get(propertyReader.getDataFileName()));
        return reader;
    }

    private Converter writeHeader(PropertyReader propertyReader) throws IOException {
        String columnFileName = propertyReader.getColumnFileName();
        String inputFileName = propertyReader.getDataFileName();
        String outputFileName = propertyReader.getResultFileName();

        Path columnFile = Paths.get(columnFileName);
        Path inputFile = Paths.get(inputFileName);
        Path outputFile = Paths.get(outputFileName);

        //set up header converter
        return HeaderWriter.writeHeader(columnFile, inputFile, outputFile);
    }

    private int getReadThreadsNumber(PropertyReader propertyReader) {
        Integer readThreads = propertyReader.getReadThreadsNumber();
        int allThreads = propertyReader.getAllThreadsNumber();

        AbstractDataReader.DataReaderImplementation implementation = propertyReader.getReaderImplementation();

        if (MULTI_ASYNC == implementation) {
            if (readThreads != null) {
                return readThreads;
            } else {
                return allThreads / 2;
            }
        } else {
            return 0;
        }
    }

    private int getFilterThreadsNumber(PropertyReader propertyReader) {
        Integer filterThreads = propertyReader.getFilterThreadsNumber();

        if (filterThreads != null) {
            return filterThreads;
        }

        int readThreads = getReadThreadsNumber(propertyReader);
        int allThreads = propertyReader.getAllThreadsNumber();

        return allThreads > readThreads ? allThreads - readThreads : readThreads;
    }

    private RowFilter getRowFilter(Map<String, String> filterData) {
        RowFilter filter = new RowIdFilter();
        ((RowIdFilter) filter).setFilterMap(filterData);
        return filter;
    }
}
