package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Column;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InputGeneratorTestIT {

    @Test
    public void testGenerator() throws IOException {

        Path dataFile = Paths.get("generate-inputs.csv");
        Path columnFile = Paths.get("generate-columns.csv");
        Path filterFile = Paths.get("generate-filter.csv");

        int dataLinesNumber = 1000;
        int filteredLinesPercent = 20;
        int columnPercent = 20;

        InputGenerator inputGenerator = new InputGenerator();
        List<Column> columns = columns();

        inputGenerator.generate(
                dataFile,
                columnFile,
                filterFile,
                columns,
                dataLinesNumber,
                true,
                columnPercent,
                filteredLinesPercent);

        int filterLines = Files.readAllLines(filterFile).size();
        Assert.assertTrue(dataLinesNumber >= filterLines);

        List<String> dataLines = Files.readAllLines(dataFile);

        List<String> ids = firstColumnValues(dataLines.subList(1, dataLines.size()));
        List<String> filteredIds = firstColumnValues(Files.readAllLines(filterFile));

        Assert.assertTrue(ids.containsAll(filteredIds));

        Files.delete(dataFile);
        Files.delete(filterFile);
        Files.delete(columnFile);
    }

    private List<String> firstColumnValues(List<String> lines) {
        return lines.stream().map(s -> Arrays.stream(s.split("\t")).findFirst().get())
                .collect(Collectors.toList());
    }

    private List<Column> columns() {
        List<String> letters = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "K");
        List<Column> result = new ArrayList<>(100);

        for (String letter: letters) {
            for (int i = 1; i <= 10; i++) {
                result.add(new Column(letter.concat(String.valueOf(i)), Column.DataType.STRING));
            }
        }

        return result;
    }
}