package com.andymur.java.pg.helpers;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static com.andymur.java.pg.readers.AbstractDataReader.DataReaderImplementation.ASYNC;
import static com.andymur.java.pg.readers.AbstractDataReader.DataReaderImplementation.BUFFERED;
import static com.andymur.java.pg.readers.AbstractDataReader.DataReaderImplementation.MULTI_ASYNC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PropertyReaderTest {

    @Test
    public void testMinimalPropertySet() throws IOException, URISyntaxException {
        int coreNumber = Runtime.getRuntime().availableProcessors();
        long xmx = Runtime.getRuntime().maxMemory();

        PropertyReader reader = buildReader("minimal-config.properties");

        assertEquals(10485760L, reader.getBlockSize());
        assertEquals(xmx / 2, reader.getAvailableMemory());


        assertEquals(coreNumber, reader.getAllThreadsNumber());
        assertNull(reader.getFilterThreadsNumber());
        assertNull(reader.getReadThreadsNumber());

        assertEquals(ASYNC, reader.getReaderImplementation());
    }

    @Test
    public void testFullPropertySet() throws IOException, URISyntaxException {
        PropertyReader reader = buildReader("config.properties");

        assertEquals(10485760L, reader.getBlockSize());
        assertEquals(3 * 1024 * 1024 * 1024L, reader.getAvailableMemory());


        assertEquals(4, reader.getAllThreadsNumber());
        assertEquals(3, (int) reader.getFilterThreadsNumber());
        assertEquals(3, (int) reader.getReadThreadsNumber());

        assertEquals(MULTI_ASYNC, reader.getReaderImplementation());
    }

    private PropertyReader buildReader(String configFileName) throws URISyntaxException, IOException {
        Path propertyFile = Paths.get(ClassLoader.getSystemResource(configFileName).toURI());

        Properties properties = new Properties();

        try (InputStream inputStream = Files.newInputStream(propertyFile)) {
            properties.load(inputStream);
        }

        return new PropertyReader(properties);
    }
}