package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Column;
import com.andymur.java.pg.domain.Row;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class RowGeneratorTest {

    @Test
    public void testGenerate() {
        RowGenerator generator = new RowGenerator(Arrays.asList(
                new Column("AAA", Column.DataType.STRING),
                new Column("BBB", Column.DataType.BOOLEAN),
                new Column("CCC", Column.DataType.INTEGER)));

        Row row = generator.generate();
        Object[] values = row.values();

        assertTrue(((String) values[0]).length() > 0);
        assertTrue(((String) values[0]).length() < 10);

        assertTrue(!((Boolean) values[1]) || (Boolean) values[1]);

        assertTrue((Integer) values[2] < Integer.MAX_VALUE && (Integer) values[2] > Integer.MIN_VALUE);
    }
}