package com.andymur.java.pg.helpers;

import com.andymur.java.pg.domain.Column;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputGeneratorRunner {

    @Test
    public void generate() throws IOException {
        int dataLinesNumber = 10_000_000;
        int filteredLinesPercent = 10;
        int columnPercent = 20;

        InputGenerator inputGenerator = new InputGenerator();
        List<Column> columns = columns();

        inputGenerator.generate(
                Paths.get("inputs.csv"),
                Paths.get("column-filter.csv"),
                Paths.get("filter.csv"),
                columns,
                dataLinesNumber,
                true,
                columnPercent,
                filteredLinesPercent);
    }

    private List<Column> columns() {
        List<String> letters = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "K");
        List<Column> result = new ArrayList<>(100);

        for (String letter: letters) {
            for (int i = 1; i <= 10; i++) {
                result.add(new Column(letter.concat(String.valueOf(i)), Column.DataType.STRING));
            }
        }

        return result;
    }
}
