package com.andymur.java.pg.transform;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class RowIdFilterTest {

    @Test
    public void testRowIdFilter() {
        Map<String, String> idFilterMap = new HashMap<>();

        idFilterMap.put("ID0", "ID1");

        RowFilter filter = new RowIdFilter();
        ((RowIdFilter) filter).setFilterMap(idFilterMap);

        List<String> values = filter.filter("ID0\tAAA\tBBB");

        Assert.assertEquals(Arrays.asList("ID1", "AAA", "BBB"), values);
    }

    @Test
    public void testRowIdFilterWhenEmpty() {
        Map<String, String> idFilterMap = new HashMap<>();

        idFilterMap.put("ID1", "ID2");

        RowFilter filter = new RowIdFilter();
        ((RowIdFilter) filter).setFilterMap(idFilterMap);

        List<String> values = filter.filter("ID0\tAAA\tBBB");

        Assert.assertEquals(Collections.emptyList(), values);
    }
}