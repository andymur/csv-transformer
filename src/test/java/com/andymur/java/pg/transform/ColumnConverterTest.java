package com.andymur.java.pg.transform;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class ColumnConverterTest {

    @Test
    public void testConvert() {
        Converter converter = new ColumnConverter();
        ((ColumnConverter) converter).setRemainedColumnIndices(Arrays.asList(1, 3));

        Optional<String> result = converter.convert(Arrays.asList("A", "B", "C", "D"));
        assertEquals("B\tD", result.get());
    }
}