package com.andymur.java.pg;

import com.andymur.java.pg.helpers.PropertyReader;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class TransformerRunnerTestIT {


    @Test
    public void testTransform() throws URISyntaxException, IOException {

        Path propertyFile = Paths.get(ClassLoader.getSystemResource("config.properties").toURI());

        Properties properties = new Properties();

        try (InputStream inputStream = Files.newInputStream(propertyFile)) {
            properties.load(inputStream);
        }

        Path expected = Paths.get(ClassLoader.getSystemResource("expected.csv").toURI());
        Path output = Paths.get(ClassLoader.getSystemResource("output.csv").toURI());

        PropertyReader propertyReader = new PropertyReader(properties);

        new Transformer().transform(propertyReader);

        filesAreEqual(expected, output);
    }

    @Test
    public void testTransformBuffered() throws URISyntaxException, IOException {

        Path propertyFile = Paths.get(
                ClassLoader.getSystemResource("config-buffered.properties"
                ).toURI());

        Properties properties = new Properties();

        try (InputStream inputStream = Files.newInputStream(propertyFile)) {
            properties.load(inputStream);
        }

        Path expected = Paths.get(ClassLoader.getSystemResource("expected.csv").toURI());
        Path output = Paths.get(ClassLoader.getSystemResource("output.csv").toURI());

        PropertyReader propertyReader = new PropertyReader(properties);

        new Transformer().transform(propertyReader);

        filesAreEqual(expected, output);
    }

    @Test
    public void testTransformAsync() throws URISyntaxException, IOException {

        Path propertyFile = Paths.get(
                ClassLoader.getSystemResource("config-async.properties"
                ).toURI());

        Properties properties = new Properties();

        try (InputStream inputStream = Files.newInputStream(propertyFile)) {
            properties.load(inputStream);
        }

        Path expected = Paths.get(ClassLoader.getSystemResource("expected.csv").toURI());
        Path output = Paths.get(ClassLoader.getSystemResource("output.csv").toURI());

        PropertyReader propertyReader = new PropertyReader(properties);

        new Transformer().transform(propertyReader);

        filesAreEqual(expected, output);
    }

    boolean filesAreEqual(Path fileA, Path fileB) throws IOException {

        Set<String> setA = new HashSet<>();
        Set<String> setB = new HashSet<>();

        try (BufferedReader readerA = Files.newBufferedReader(fileA);
             BufferedReader readerB = Files.newBufferedReader(fileB)) {

            String lineA;
            String lineB;

            do {

                lineA = readerA.readLine();
                lineB = readerB.readLine();

                if (lineA == null && lineB == null) {
                    break;
                }

                if ((lineA == null) || (lineB == null)) {
                    return false;
                }

                setA.add(lineA);
                setB.add(lineB);

            } while (true);

            return setA.equals(setB);
        }
    }
}