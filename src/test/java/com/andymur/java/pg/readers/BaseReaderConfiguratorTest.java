package com.andymur.java.pg.readers;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseReaderConfiguratorTest {

    @Test
    public void testConfigure() throws IOException, URISyntaxException {
        //File approx. 22MB
        Path inputFile = Paths.get(ClassLoader.getSystemResource("inputs.csv").toURI());

        BaseReaderConfigurator configurator = new BaseReaderConfigurator();
        configurator.setAvailableMemory(3 * 1024 * 1024 * 1024L);
        configurator.setBlockSize(10 * 1024 * 1024);

        configurator.configure(inputFile);

        assertEquals(3, configurator.getBlocksNumber());
        assertTrue(configurator.getFileSize() == configurator.getAvailableMemory());
    }

    @Test
    public void testConfigureLargeFile() throws IOException, URISyntaxException {
        //File approx. 22MB
        Path inputFile = Paths.get(ClassLoader.getSystemResource("inputs.csv").toURI());

        BaseReaderConfigurator configurator = new BaseReaderConfigurator();
        configurator.setAvailableMemory(10 * 1024 * 1024L);
        configurator.setBlockSize(5 * 1024 * 1024);

        configurator.configure(inputFile);

        assertEquals(5, configurator.getBlocksNumber());
        assertEquals(10 * 1024 * 1024L, configurator.getAvailableMemory());
    }
}